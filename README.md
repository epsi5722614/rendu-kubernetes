# Rendu TP Kubernetes

Ceci est le rendu du TP Kubernetes.

# Réponses aux questions

1. "ClusterIP" va donner une IP adresse pour une communication entre les différentes clusters, alors que "NodePort" va exposer les ports, et donc les applications du cluster, aux clients externes.

2. On choisirait plutôt du scaling vertical : un des enjeux d'une BDD est la rapidité d'accès à la donnée. Il est plus rapide d'accéder à la donnée sur un serveur plus puissant, plutôt que de recouper l'information dans plusieurs serveurs (plus long temps d'accès). Cela se fera par l'ajout de disques durs, de RAM et de puissance processeur.

3. Un cloud privé offrira plus de contrôle, plus de customisation sur la solution. Cela découle directement du fait que la plupart (si ce n'est l'immense majorité) de ces clouds privés résident au sein même de l'infrastructure de l'organisation. Cela offre également une sécurité grandement accrue, car la surveillance et le contrôle des accès à ce cloud peuvent se faire de façon plus fine par l'organisation.

4. Une instance AWS peut être avantageuse pour une organisation ne disposant pas de ressources à allouer dans la création d'un cloud privé. Les grands intérêts à aller voir chez AWS peut se voir sous au moins trois aspects : la performance, le coût et la sécurité. Niveau performance, Amazon met à disposition des services Cloud extrêmement performants et très accessibles (que ce soit au niveau du service level ou de l'utilisation). Créer un cloud privé est un coût non négligeable : AWS propose un "free tier", ainsi que l'option "Pay as you go".
