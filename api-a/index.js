const express = require("express");
const http = require("http");
const app = express();
const port = 30000;

app.get("/", (req, res) => {
  callApiB();
});

data = "";

function callApiB() {
  http.get("http://127.0.0.1:30001", (res) => {
    res.on("data", (chunk) => {
      data += chunk;
    });
    res.on("end", () => {
      console.log(data);
    });
  });
}

app.listen(port, () => {
  console.log(`API A - ${port}`);
});
