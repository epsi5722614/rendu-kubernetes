const express = require("express");
const app = express();
const port = 30001;

app.get("/", (req, res) => {
  res.send("Je suis la valeur retournée par l'API B.");
});

app.listen(port, () => {
  console.log(`API B - ${port}`);
});
